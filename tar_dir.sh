#!/bin/bash
CUR_DIR=$(pwd)
echo "Executing in $CUR_DIR"
cd "$1"
echo "Target folder: $(pwd)"
TARGET=$(basename $(pwd))
TARGET_SHORT=${TARGET::-7}
FULL_TGZ_PATH="${CUR_DIR}/tgz_new/${TARGET_SHORT}.tgz"
echo "Target path: ${FULL_TGZ_PATH}"
tar -czvf "${FULL_TGZ_PATH}" *
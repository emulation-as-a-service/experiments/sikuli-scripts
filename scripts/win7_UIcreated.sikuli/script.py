# EAAS Configuration Block Start
# Do not change!
SC_COUNT = 0
def sc():
    global SC_COUNT
    wait(1)
    img_name = "sc" + str(SC_COUNT) + ".png"
    print("[SIKULI SCRIPT] Screenshot:", img_name)
    cap = SCREEN.capture()
    img = Image(cap)
    img.save(img_name)
    SC_COUNT += 1

print("[SIKULI SCRIPT] Starting Sikuli Script...")
SCREEN.setAutoWaitTimeout(30)
# EAAS Configuration Block End
click("img_0.png")
wait(3)
click("img_1.png")
wait(3)
type("cmd")
click("img_2.png")
wait(3)
wait("img_3.png")
wait(1)
type("notepad abc.txt")
type(Key.ENTER)
wait("img_4.png")
wait(3)
type(Key.ENTER)
wait(3)
click(Pattern("img_5.png").targetOffset(-6,12))
wait(3)
type("abcdef")
wait(3)
type("s", Key.CTRL)
wait(3)
click("img_0.png")
click("img_6.png")
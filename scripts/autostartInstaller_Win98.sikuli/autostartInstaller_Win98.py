DEBUG = False

def sc(img_name):
    if not DEBUG:
        return
    wait(2)
    print("[SIKULI SCRIPT] Screenshot:", img_name)
    cap = SCREEN.capture()
    img = Image(cap)
    img.save(img_name)
print("[SIKULI SCRIPT] Starting Sikuli Script: Installing Autostart Win 98")

print(sys.argv)
if len(sys.argv) > 1:
    DEBUG = True
    print("DEBUG MODE ENABLED")

SCREEN.setAutoWaitTimeout(30)
wait(4)
sc("sc0.png")
doubleClick("1646233838528.png")
wait(15)
sc("sc1.png")

already_found = False
try:
    cde = find("1646820516880.png")
    print("Found CDE")
    doubleClick(cde)
    already_found = True   

except:
    print("Did not find C, D, E next to each other")


if not already_found:
    try:
        d = find(Pattern("1650881061224.png").similar(0.85))
        doubleClick(d)
    
    except:
        print("Did not find Cdrom (D:)")
    
wait(4)
sc("sc2.png")
doubleClick("1646233892597.png")
wait(4)
sc("sc3.png")
click("1646759858319.png")
click("1646759870271.png")
click("1646759880418.png")
wait(10)
sc("sc4.png") # TODO when win 98 does not shut down properly, check screenshot and send shutdown request

print("Done")
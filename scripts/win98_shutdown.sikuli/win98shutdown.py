DEBUG = False
SC_COUNT = 0

def sc():
    global DEBUG
    global SC_COUNT
    
    if not DEBUG:
        return
    wait(4)
    img_name = "sc" + str(SC_COUNT) + ".png"
    print("[SIKULI SCRIPT] Screenshot:", img_name)
    cap = SCREEN.capture()
    img = Image(cap)
    img.save(img_name)
    SC_COUNT += 1

print("[SIKULI SCRIPT] Starting Sikuli Script: Shutdown Win 98")

print(sys.argv)
if len(sys.argv) > 1:
    DEBUG = True
    print("DEBUG MODE ENABLED")

SCREEN.setAutoWaitTimeout(30)

sc()
wait(2)
click("1646759858319.png")
click("1646759870271.png")
sc()
click("1646759880418.png")
print("Done")
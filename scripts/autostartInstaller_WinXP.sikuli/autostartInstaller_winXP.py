# EAAS Configuration Block Start
# Do not change!
SC_COUNT = 0
def sc():
    global SC_COUNT
    wait(1)
    img_name = "sc" + str(SC_COUNT) + ".png"
    print("[SIKULI SCRIPT] Screenshot:", img_name)
    cap = SCREEN.capture()
    img = Image(cap)
    img.save(img_name)
    SC_COUNT += 1

print("[SIKULI SCRIPT] Starting Sikuli Script...")
SCREEN.setAutoWaitTimeout(30)
# EAAS Configuration Block End

print("[SIKULI SCRIPT] Starting Sikuli Script: Installing Autostart Win XP")
click("1646221984618-123.png")
sc("sc0.png")
click("1646221995474.png")
sc("sc1.png")
if exists(Pattern("1682503580820.png").similar(0.75)):
        doubleClick(Pattern("1682503580820.png").similar(0.75))
elif exists(Pattern("1646914642468.png").similar(0.96)):
    doubleClick(Pattern("1646914642468.png").similar(0.96))

else:
    print("Could not find CDROM Drive!")
    raise Exception("Could not find CDROM Drive!")
sc("sc2.png")
doubleClick(Pattern("1646919775277.png").similar(0.91))
wait(70) # necessary!
sc("sc6.png")

click("1646221984618-123.png")
click("1646222342171.png")
click("1646222359914.png")
print("Done")
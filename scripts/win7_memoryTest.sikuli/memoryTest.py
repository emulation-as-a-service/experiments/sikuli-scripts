DEBUG = False
SC_COUNT = 0

def sc():
    global DEBUG
    global SC_COUNT
    
    if not DEBUG:
        return
    wait(4)
    img_name = "sc" + str(SC_COUNT) + ".png"
    print("[SIKULI SCRIPT] Screenshot:", img_name)
    cap = SCREEN.capture()
    img = Image(cap)
    img.save(img_name)
    SC_COUNT += 1

print("[SIKULI SCRIPT] Starting Sikuli Script: Create Text File and write arguments")
SCREEN.setAutoWaitTimeout(30)

print(sys.argv)
if len(sys.argv) > 1:
    DEBUG = True
    print("DEBUG MODE ENABLED")

wait(3)
sc()
click("1656585641246.png")
wait(3)
sc()
click("1657117258406.png")
wait(5)
type("cmd")
wait(2)
sc()
click("1657117279927.png")
wait("1657117306199.png")
wait(1)
type("notepad abc.txt")
wait(2)
type(Key.ENTER)
wait("1657117353598.png")
type(Key.ENTER)
wait(1)
click("1657117377097.png")
sc()


for i in range(100):
    type(str(i))
    type(Key.ENTER)
    sc()
    wait(1)

wait(1)
type("s", KEY_CTRL)
wait(3)

click("1656585641246.png")
click("1656586580989.png")
print("Done!")
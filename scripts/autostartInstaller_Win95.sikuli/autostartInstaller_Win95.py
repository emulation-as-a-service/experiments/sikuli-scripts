DEBUG = False

def sc(img_name):
    if not DEBUG:
        return
    wait(2)
    print("[SIKULI SCRIPT] Screenshot:", img_name)
    cap = SCREEN.capture()
    img = Image(cap)
    img.save(img_name)
print("[SIKULI SCRIPT] Starting Sikuli Script: Installing Autostart Win 95")

print(sys.argv)
if len(sys.argv) > 1:
    DEBUG = True
    print("DEBUG MODE ENABLED")

SCREEN.setAutoWaitTimeout(30)
wait(4)
sc("sc0.png")

x1 = find("1652257649984.png")
print("1: x " + str(x1.x)) 
print("y: " + str( x1.y)) 
doubleClick("1652257649984.png")
wait(10)
sc("sc1.png")
x2 = find("1652258159327.png")
print("2: x " + str(x2.x)) 
print("y: " + str( x2.y)) 
doubleClick("1652258159327.png")
wait(10)
sc("sc2.png")
doubleClick("1652258184765.png")
sc("sc3.png")
click("1652258221218.png")
click("1652258241779.png")
click("1652258279911.png")
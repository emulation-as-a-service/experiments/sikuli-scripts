# EAAS Configuration Block Start
# Do not change!
SC_COUNT = 0
def sc():
    global SC_COUNT
    wait(1)
    img_name = "sc" + str(SC_COUNT) + ".png"
    print("[SIKULI SCRIPT] Screenshot:", img_name)
    cap = SCREEN.capture()
    img = Image(cap)
    img.save(img_name)
    SC_COUNT += 1

print("[SIKULI SCRIPT] Starting Sikuli Script...")
SCREEN.setAutoWaitTimeout(30)
# EAAS Configuration Block End
print("[SIKULI SCRIPT] Starting Sikuli Script: Installing Sublime Text")
wait(2)
click("1646221984618.png")
click(Pattern("1686568185064.png").similar(0.90))
sc()
wait("1686568223504.png")
sc()
type("Test File 123")
type(Key.ENTER)
type("EAAS!")
sc()
wait(1)
type("s", KEY_CTRL)
wait("1686568313867.png")
type("testfile.txt")
sc()
wait(1)
click("1686568313867.png")
sc()
wait(3)
click("1646221984618.png")
click("1646222342171.png")
click("1646222359914.png")
print("Done, output: testfile.txt")







DEBUG = False
SC_COUNT = 0

def sc():
    global DEBUG
    global SC_COUNT
    
    if not DEBUG:
        return
    wait(4)
    img_name = "sc" + str(SC_COUNT) + ".png"
    print("[SIKULI SCRIPT] Screenshot:", img_name)
    cap = SCREEN.capture()
    img = Image(cap)
    img.save(img_name)
    SC_COUNT += 1

print("[SIKULI SCRIPT] Starting Sikuli Script: Sharepoint Import")
SCREEN.setAutoWaitTimeout(30)

print(sys.argv)
if len(sys.argv) > 1:
    DEBUG = True
    print("DEBUG MODE ENABLED")


sc()
wait("1653472764758.png")

type(Key.DELETE,KEY_CTRL | KEY_ALT)

sc()

wait("1653474121246.png")

sc()
type("Passw0rd")
wait(2)
type(Key.ENTER)

sc()

click("1653473057701.png")
click("1653473071485.png")
sc()
wait("1653473119700.png")

type("cd c:\\Program Files\\Common Files\\Microsoft Shared\\web server extensions\\12\\bin")
wait(2)
type(Key.ENTER)

wait(5)
sc()

type("STSADM.EXE -o import -url http://winserver2003en -filename e:\\export1.cmp")
wait(2)
type(Key.ENTER)

sc()
wait(10)
sc()
wait(30)
sc()

wait("1653911002036.png", 600)

wait(5)

click("1653473057701.png")
click("1653482725380.png")
click("1653482750507.png")
wait(3)
sc()
type("sikuli shutdown")
wait(3)

click("1653482781736.png")
print("Created " + str(SC_COUNT) + " screenshots!")




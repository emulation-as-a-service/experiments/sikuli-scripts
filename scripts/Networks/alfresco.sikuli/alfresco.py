# EAAS Configuration Block Start
# Do not change!
SC_COUNT = 0
def sc():
    global SC_COUNT
    wait(1)
    img_name = "sc" + str(SC_COUNT) + ".png"
    print("[SIKULI SCRIPT] Screenshot:", img_name)
    cap = SCREEN.capture()
    img = Image(cap)
    img.save(img_name)
    SC_COUNT += 1

print("[SIKULI SCRIPT] Starting Sikuli Script...")
SCREEN.setAutoWaitTimeout(300)
# EAAS Configuration Block End

print("[SIKULI SCRIPT] Starting Sikuli Script: Alfresco File Download")

SEARCH_TERM = "txt"
MAX_RESULTS = 100000
if len(sys.argv) > 2:
    SEARCH_TERM = sys.argv[2]htt
    print("Setting search term to:", SEARCH_TERM)

if len(sys.argv) > 3:
    MAX_RESULTS = int(sys.argv[3])
    print("Setting max results to:", MAX_RESULTS)

wait(600)
sc()
wait("1655822657298.png")
sc()
click("1655817226365.png")
wait(3)
sc()
wait("1655817340105.png")
type("http://alfrescostb.com:8080/alfresco")
wait(3)
sc()
wait(1)
type(Key.ENTER)
print("Waiting 20 secs for website to load...")
wait(20)
sc()
click("1655817674106.png")
wait("1655817698295.png")
sc()
type("admin")
wait(1)
type(Key.TAB)
wait(1)
type("admin")
wait(1)
type(Key.ENTER)
sc()
wait (1)

if exists("1655817786284.png"):
    click("1655817786284.png")
    wait(1)
sc()
click("1655817813224.png")
sc()
wait(1)
type("disposing of digital debris")
wait(1)
type(Key.ENTER)
wait("1655821465067.png")
sc()
wait(1)

result_count = 0

def download_results():
    print("Downloading results")
    global result_count
    global MAX_RESULTS
    sc()

    for i in findAll(Pattern("1655820265498.png").similar(0.75)):

        print("Result count:", result_count)
        if result_count >= MAX_RESULTS:
            print("Reached max results:", MAX_RESULTS)
            return
        
        click(i)
        wait("1655820686051.png")
        type("s", Key.ALT)
        wait(1)
        type(Key.ENTER)
        sc()
        wait(3)
        if exists("1655820366339.png"):
            t = find("1655820366339.png").right().find("1655820489457.png")
            click(t)
        wait(1)
        result_count += 1
    print("Done with downloading...")


download_results()

while result_count < MAX_RESULTS:

    sc()
    print("result count was below max results, checking for next page")
    content_items = find("1658751545517.png")

    if content_items.below().exists(Pattern("1658751804629.png").similar(0.95).targetOffset(-10,0)):
        print("Found one more page")
        click(content_items.below().find(Pattern("1658751804629.png").similar(0.95).targetOffset(-10,0)))

        wait(10)

        download_results()
    else:
        break
    
wait(10)
click("1655822657298.png")
click("1655822665996.png")
print("Done!")




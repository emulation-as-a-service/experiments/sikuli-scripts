DEBUG = False
SC_COUNT = 0

def sc():
    global DEBUG
    global SC_COUNT
    
    if not DEBUG:
        return
    wait(4)
    img_name = "sc" + str(SC_COUNT) + ".png"
    print("[SIKULI SCRIPT] Screenshot:", img_name)
    cap = SCREEN.capture()
    img = Image(cap)
    img.save(img_name)
    SC_COUNT += 1

print("[SIKULI SCRIPT] Starting Sikuli Script: Alfresco File Download")
SCREEN.setAutoWaitTimeout(30)

print("Arguments:", sys.argv)
if len(sys.argv) > 1:
    DEBUG = True
    print("DEBUG MODE ENABLED")

if len(sys.argv) > 2:
    writeString = sys.argv[2]

wait(3)

wait("1656585641246.png")
wait(5)
rightClick("1656585680851.png")
click(Pattern("1656587389565.png").similar(0.80).targetOffset(32,-23))
click("1656586334609.png")
wait(2)
type("testDoc")
wait(1)
type(Key.ENTER)
click("1656585641246.png")
click("1656586580989.png")
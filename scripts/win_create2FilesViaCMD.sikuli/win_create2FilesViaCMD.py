DEBUG = True
SC_COUNT = 0

def sc():
    global DEBUG
    global SC_COUNT
    
    if not DEBUG:
        return
    wait(2)
    img_name = "sc" + str(SC_COUNT) + ".png"
    print("[SIKULI SCRIPT] Screenshot:", img_name)
    cap = SCREEN.capture()
    img = Image(cap)
    img.save(img_name)
    SC_COUNT += 1
    wait(1)



print("[SIKULI SCRIPT] Starting Sikuli Script: Creating 2 Files via CMD")
SCREEN.setAutoWaitTimeout(30)
sc()
print("Sending R + WIN")
type("r", KeyModifier.WIN)
sc()
type("cmd")
sc()
type ("echo abc >> test1.txt")
wait(1)
type(Key.ENTER)
wait(1)
type ("echo xyz >> test2.json")
wait(1)
type(Key.ENTER)
sc()
type ("shutdown -s -t 5")
wait(1)
type(Key.ENTER)
sc()
print("Done!")
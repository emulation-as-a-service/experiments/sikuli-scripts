#!/bin/bash
CUR_DIR=$(pwd)
echo "Executing in $CUR_DIR"
cd "$1"
echo "Target folder: $(pwd)"
TARGET=$(basename $(pwd))
TARGET_SHORT=${TARGET::-7}
FULL_TGZ_PATH="${CUR_DIR}/tars/${TARGET_SHORT}.tar"
echo "Target path: ${FULL_TGZ_PATH}"
tar -cvf "${FULL_TGZ_PATH}" *